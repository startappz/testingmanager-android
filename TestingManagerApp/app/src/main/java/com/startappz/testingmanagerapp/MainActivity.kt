package com.startappz.testingmanagerapp

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.startappz.testingmanagersdk.TestingManager
import com.startappz.testingmanagersdk.TestingManagerImp
import com.startappz.testingmanagersdk.extentions.click
import com.startappz.testingmanagersdk.shake_detector.ShakeManager
import com.startappz.testingmanagersdk.shake_detector.ShakeManagerImp
import com.testingmanagerapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var testingManager: TestingManager
    lateinit var binding: ActivityMainBinding
    private var shakeManager: ShakeManager = ShakeManagerImp()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupListeners()
        testingManager = TestingManagerImp(
            this,
            overrideFeaturesEnabled = true,
            featureBaseUrl = true
        )
        testingManager.bindActionOne("Test1") {
            Toast.makeText(
                this,
                "CLICKED TEST1",
                Toast.LENGTH_SHORT
            ).show()
        }
        testingManager.bindActionTwo("Test2") {
            Toast.makeText(
                this,
                "CLICKED TEST2",
                Toast.LENGTH_SHORT
            ).show()
        }
        testingManager.bindActionThree("Test3") {
            Toast.makeText(
                this,
                "CLICKED TEST3",
                Toast.LENGTH_SHORT
            ).show()
        }

        testingManager.bindToggle("Toggle One", "ToggleOne") {
            print("Toggle One $it")
        }
        testingManager.bindToggleTwo("Toggle Two", "ToggleTwo") {
            print("Toggle Two $it")
        }

        testingManager.bindFeatures(listOf("One", "Two", "Three", "Four"))

        testingManager.bindSelector("Env switch", "This is title", listOf("One", "Two", "Free")){
            Log.d("!!!", "State Selector ${testingManager.isSelectorEnabled()} Selected item ${testingManager.getSelectorItem()}")
        }

        shakeManager.bind(this) { showTestingManager() }
    }

    private fun setupListeners() {
        binding.btnClickMe.click {
            showTestingManager()
        }

        binding.btnCheck.click {
            Toast.makeText(
                this,
                "Features are override ${testingManager.isFeatureOverride("One")}\nFeature State flag ${testingManager.getFeatureFlagState("One")}",
                Toast.LENGTH_SHORT
            ).show()

            Log.d("!!!", "MOCK MANAGER : ${testingManager.isMockApiEnabled()}")
            Log.d("!!!", "MOCK TYPE : ${testingManager.getApiMockType()}")
            Log.d("!!!", "MOCK Version : ${testingManager.getApiMockVersion()}")
            Log.d("!!!", "BASE URL OVERRIDE : ${testingManager.isBaseUrlOverride()}")
            Log.d("!!!", "BASE URL : ${testingManager.getBaseUrl()}")
            Log.d("!!!", "FEATURE MANAGER ${testingManager.isFeatureOverride("One")}")
            Log.d("!!!", "State Selector ${testingManager.isSelectorEnabled()} Selected item ${testingManager.getSelectorItem()}")
        }
    }

    private fun showTestingManager() {
        testingManager.showTestingManager(supportFragmentManager)
    }

    override fun onResume() {
        super.onResume()
        shakeManager.register()
    }

    override fun onPause() {
        shakeManager.unregister()
        super.onPause()
    }
}
