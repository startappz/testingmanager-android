package com.startappz.testingmanagersdk.dialog

import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RadioButton
import com.startappz.testingmanagersdk.base.AdjustDialog
import com.startappz.testingmanagersdk.base.BaseDialogFragment
import com.startappz.testingmanagersdk.constants.DIALOG_LIST
import com.startappz.testingmanagersdk.constants.DIALOG_SELECTOR_PREF
import com.startappz.testingmanagersdk.constants.DIALOG_TITLE
import com.startappz.testingmanagersdk.databinding.DialogSelectorBinding
import com.startappz.testingmanagersdk.helpers.PreferenceHelper
import com.startappz.testingmanagersdk.helpers.PreferenceHelper.get
import com.startappz.testingmanagersdk.helpers.PreferenceHelper.set

class DialogSelector : BaseDialogFragment<DialogSelectorBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> DialogSelectorBinding
        get() = DialogSelectorBinding::inflate

    override val adjustDialog = AdjustDialog.WRAP

    var onDismiss: () -> Unit = {}

    override fun setupView() {
        arguments?.let {
            val title = it.getString(DIALOG_TITLE)
            val items = it.getSerializable(DIALOG_LIST) as List<String>
            val currentItems = PreferenceHelper.defaultPrefs(requireContext())[DIALOG_SELECTOR_PREF, ""]
            bindList(items, currentItems)

            binding.title.text = title
        }
    }

    override fun setupListeners() {
        binding.actionOk.setOnClickListener { dismissAllowingStateLoss() }
        binding.group.setOnCheckedChangeListener { radioGroup, i ->
            val btn = radioGroup.findViewById<RadioButton>(i)
            PreferenceHelper.defaultPrefs(requireContext())[DIALOG_SELECTOR_PREF] = btn.tag
        }
    }

    private fun bindList(items: List<String>?, current: String?) {
        if (items.isNullOrEmpty()) return

        items.forEach {
            val radioButton = RadioButton(requireContext())
            radioButton.text = it
            radioButton.tag = it
            binding.group.addView(radioButton)
        }

        for (i in items.indices) {
            val child = binding.group.getChildAt(i) as RadioButton
            if (child.text == current) {
                child.isChecked = true
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismiss()
    }
}
