package com.startappz.testingmanagersdk.dialog

import android.view.LayoutInflater
import android.view.ViewGroup
import com.startappz.testingmanagersdk.base.AdjustDialog
import com.startappz.testingmanagersdk.base.BaseDialogFragment
import com.startappz.testingmanagersdk.controller.AvailableFeatureFlagController
import com.startappz.testingmanagersdk.databinding.DialogSelectFeatureBinding
import com.startappz.testingmanagersdk.extentions.afterTextChanged

class FeatureOverrideAddDialog :
    BaseDialogFragment<DialogSelectFeatureBinding>() {

    var featureFlags: List<String>? = null
    var onItemAddFeatureFlag: (String) -> Unit = {}
    lateinit var controller: AvailableFeatureFlagController

    override val adjustDialog = AdjustDialog.FULL

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> DialogSelectFeatureBinding
        get() = DialogSelectFeatureBinding::inflate

    override fun setupView() {
        controller = AvailableFeatureFlagController(onItemAddFeatureFlag)
        binding.recyclerFeatureFlags.setController(controller)
        controller.setFlags(featureFlags)
    }

    override fun setupListeners() {
        binding.btnClose.setOnClickListener { dismissAllowingStateLoss() }
        binding.editSearch.afterTextChanged { search(it) }
    }

    private fun search(input: CharSequence) {
        val outPut = featureFlags?.filter { filter -> filter.lowercase().contains(input.toString().lowercase()) }?.toList()

        controller.setFlags(outPut)
    }
}
