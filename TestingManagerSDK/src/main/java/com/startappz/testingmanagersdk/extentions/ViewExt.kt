package com.startappz.testingmanagersdk.extentions

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.CompoundButton
import android.widget.EditText

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun View.click(func: () -> Unit) {
    setOnClickListener { func() }
}

fun CompoundButton.checkChange(func: (Boolean) -> Unit) {
    setOnCheckedChangeListener { _, state -> func(state) }
}

fun View?.toGone() {
    if (this == null) return
    if (visibility == View.GONE) return
    visibility = View.GONE
}

fun View?.toVisible() {
    if (this == null) return
    if (visibility == View.VISIBLE) return
    visibility = View.VISIBLE
}

var View.visible
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value)
            View.VISIBLE
        else
            View.GONE
    }

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}
