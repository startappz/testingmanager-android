package com.startappz.testingmanagersdk

import androidx.fragment.app.FragmentManager

interface TestingManager {

    fun bindFeatures(features: List<String>)

    fun showTestingManager(fragmentManager: FragmentManager)

    fun isFeatureOverride(featureFlag: String): Boolean

    fun isFeatureOverrideAllowed(): Boolean

    fun isFeatureOverrideEnabled(): Boolean

    fun getFeatureFlagState(featureFlag: String): Boolean

    fun isMockApiEnabled(): Boolean

    fun getApiMockType(): MockType

    fun getApiMockVersion(): String

    fun isBaseUrlOverride(): Boolean

    fun isFeatureBaseURLEnabled(): Boolean

    fun setBaseURLOverrideEnabled(state: Boolean)

    fun setBaseURL(baseURL: String)

    fun getBaseUrl(): String

    fun setMockApiState(enabled: Boolean)

    fun bindActionOne(actionText: String, callback: () -> Unit)

    fun bindActionTwo(actionText: String, callback: () -> Unit)

    fun bindActionThree(actionText: String, callback: () -> Unit)

    fun bindToggle(actionText: String, keyTogglePreference: String, callback: (Boolean) -> Unit)

    fun bindToggleTwo(actionText: String, keyTogglePreference: String, callback: (Boolean) -> Unit)

    fun bindSelector(actionText: String, title: String, list: List<String>,callback: () -> Unit)

    fun isSelectorEnabled(): Boolean

    fun getSelectorItem(): String

    fun getToggleState(): Boolean

    fun setToggleState(state: Boolean)

    fun setApiMockVersion(version: String?)

    fun getFeatures(): List<String>

    fun removeFeatureFlagOverride(featureFlag: String)

    fun setFeatureFlagState(featureFlag: String, state: Boolean)

    fun getFeatureFlagsOverride(): Map<String, Boolean>

    fun setFeatureOverrideState(enabled: Boolean)

    fun setToggleTwoState(state: Boolean)

    fun getToggleTwoState(): Boolean
}
