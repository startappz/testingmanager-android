package com.startappz.testingmanagersdk

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.EpoxyTouchHelper
import com.startappz.testingmanagersdk.constants.DIALOG_LIST
import com.startappz.testingmanagersdk.constants.DIALOG_TITLE
import com.startappz.testingmanagersdk.constants.TOGGLE_SELECTOR_STATE
import com.startappz.testingmanagersdk.controller.FeatureOverrideController
import com.startappz.testingmanagersdk.databinding.FragmentTestingManagerControlBinding
import com.startappz.testingmanagersdk.dialog.DialogSelector
import com.startappz.testingmanagersdk.dialog.FeatureOverrideAddDialog
import com.startappz.testingmanagersdk.extentions.*
import com.startappz.testingmanagersdk.helpers.PreferenceHelper
import com.startappz.testingmanagersdk.helpers.PreferenceHelper.set
import com.startappz.testingmanagersdk.model.FeatureFlagOverrideModel
import com.startappz.testingmanagersdk.model.FeatureFlagOverrideModel_

class TestingManagerDialog : DialogFragment() {
    private lateinit var binding: FragmentTestingManagerControlBinding
    private var testingManager: TestingManager? = null
    private lateinit var featureController: FeatureOverrideController
    private var onDismissed: () -> Unit = {}

    // Custom items
    private var actionText1: String? = null
    private var action1: () -> Unit = {}
    private var actionText2: String? = null
    private var action2: () -> Unit = {}
    private var actionText3: String? = null
    private var action3: () -> Unit = {}
    private var actionTextToggle: String? = null
    private var actionToggle: (Boolean) -> Unit = {}

    private var actionToggleTwo: (Boolean) -> Unit = {}
    private var actionTextToggleTwo: String? = null

    // Selector
    private var selectorTitle: String? = null
    private var actionSelectorText: String? = null
    private var selectorList: List<String>? = null
    private var selectorCallbackState: () -> Unit = {}

    fun bind(
        testingManager: TestingManager,
        actionText1: String? = null,
        action1: () -> Unit = {},
        actionText2: String? = null,
        action2: () -> Unit = {},
        actionText3: String? = null,
        action3: () -> Unit = {},
        actionTextToggle: String? = null,
        actionToggle: (Boolean) -> Unit = {},
        actionToggleTwo: (Boolean) -> Unit = {},
        actionTextToggleTwo: String? = null,
        selectorTitle: String? = null,
        actionSelectorText: String? = null,
        selectorList: List<String>? = null,
        selectorCallbackState: () -> Unit = {},
        onDismissed: () -> Unit
    ): TestingManagerDialog {
        this.testingManager = testingManager

        this.actionText1 = actionText1
        this.action1 = action1
        this.actionText2 = actionText2
        this.action2 = action2
        this.actionText3 = actionText3
        this.action3 = action3
        this.actionTextToggle = actionTextToggle
        this.actionTextToggleTwo = actionTextToggleTwo
        this.actionToggle = actionToggle
        this.actionToggleTwo = actionToggleTwo
        this.onDismissed = onDismissed
        this.selectorTitle = selectorTitle
        this.actionSelectorText = actionSelectorText
        this.selectorList = selectorList
        this.selectorCallbackState = selectorCallbackState
        return this
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTestingManagerControlBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
        setupPreselectedState()
        bindFeatureSection()
    }

    private fun setupListeners() {
        // Mock API Section
        with(binding) {
            switchEnableMock.checkChange { switchEnableMock(it) }

            etVersion.afterTextChanged { text ->
                testingManager?.setApiMockVersion(text.trim())
            }
            // BASE URL SECTION
            switchOverrideUrl.checkChange { switchEnableBaseUrl(it) }
            etBaseUrl.afterTextChanged { text -> testingManager?.setBaseURL(text.trim()) }

            // Feature Section
            switchEnableFeature.checkChange { state ->
                switchEnableFeature(state)
            }

            // Selector Feature
            toggleSelector.checkChange {
                PreferenceHelper.defaultPrefs(requireContext())[TOGGLE_SELECTOR_STATE] = it
                selectorCallbackState()
            }
            btnSelectFeatureFlag.click { showAddFeatureDialog() }
        }
    }

    private fun setupPreselectedState() {
        with(binding) {
            switchEnableMock.isChecked = testingManager?.isMockApiEnabled() ?: false
            switchOverrideUrl.isChecked = testingManager?.isBaseUrlOverride() ?: false
            epoxyAddedFlags.visible = testingManager?.isFeatureOverrideEnabled() ?: false
            featureSection.visible = testingManager?.isFeatureOverrideAllowed() ?: false
            etVersion.setText(testingManager?.getApiMockVersion())
            etBaseUrl.setText(testingManager?.getBaseUrl())
            switchEnableFeature.isChecked = testingManager?.isFeatureOverrideEnabled() ?: false
            toggleSelector.isChecked = testingManager?.isSelectorEnabled() ?: false
        }

        bindActionOne(actionText1, action1)
        bindActionTwo(actionText2, action2)
        bindActionThree(actionText3, action3)
        bindToggle(actionTextToggle, actionToggle)
        bindToggleTwo(actionTextToggleTwo, actionToggleTwo)
        bindSelector()
    }

    private fun switchEnableMock(state: Boolean) {
        testingManager?.setMockApiState(state)
        when {
            state -> {
                binding.etVersion.toVisible()
                binding.lblMockVersion.toVisible()
                binding.switchOverrideUrl.visible =
                    state && testingManager?.isFeatureBaseURLEnabled() ?: false
            }
            else -> {
                binding.etVersion.toGone()
                binding.lblMockVersion.toGone()
                binding.switchOverrideUrl.visible =
                    state && testingManager?.isFeatureBaseURLEnabled() ?: false
                binding.switchOverrideUrl.isChecked = false
            }
        }
    }

    private fun switchEnableBaseUrl(state: Boolean) {
        testingManager?.setBaseURLOverrideEnabled(state)
        when {
            state -> {
                binding.etBaseUrl.toVisible()
            }
            else -> {
                binding.etBaseUrl.toGone()
            }
        }
    }

    private fun switchEnableFeature(state: Boolean) {
        testingManager?.setFeatureOverrideState(state)
        if (state) {
            binding.tvFeatureOverrideDescription.toVisible()
            binding.btnSelectFeatureFlag.toVisible()
            binding.epoxyAddedFlags.toVisible()
        } else {
            binding.tvFeatureOverrideDescription.toGone()
            binding.btnSelectFeatureFlag.toGone()
            binding.epoxyAddedFlags.toGone()
        }
    }

    private fun showAddFeatureDialog() {
        testingManager?.let { manager ->
            val filteredFeature = manager.getFeatures().filter { filter ->
                !featureController.featureFlagsOverride.containsKey(TestingManagerImp.KEY_FEATURE_OVERRIDE_PREFIX + filter)
            }

            val featureDialog = FeatureOverrideAddDialog()
            featureDialog.featureFlags = filteredFeature
            featureDialog.onItemAddFeatureFlag = { f ->
                featureDialog.dismissAllowingStateLoss()
                manager.setFeatureFlagState(f, false)
                featureController.setFeatures(manager.getFeatureFlagsOverride())
            }
            activity?.let { act ->
                featureDialog.show(
                    act.supportFragmentManager,
                    FeatureOverrideAddDialog::javaClass.name
                )
            }
        }
    }

    private fun bindFeatureSection() {
        testingManager?.let { manager ->
            featureController = FeatureOverrideController { feature, state ->
                manager.setFeatureFlagState(feature, state)
            }
            binding.epoxyAddedFlags.setController(featureController)
            featureController.setFeatures(manager.getFeatureFlagsOverride())

            EpoxyTouchHelper.initSwiping(binding.epoxyAddedFlags).left().withTargets(
                FeatureFlagOverrideModel_::class.java
            )
                .andCallbacks(object : EpoxyTouchHelper.SwipeCallbacks<EpoxyModel<*>>() {
                    override fun onSwipeCompleted(
                        model: EpoxyModel<*>,
                        itemView: View,
                        position: Int,
                        direction: Int
                    ) {
                        // Remove swiped item from list and notify the RecyclerView
                        val featureFlag = (model as FeatureFlagOverrideModel).featureFlag
                        manager.removeFeatureFlagOverride(featureFlag)
                        featureController.setFeatures(manager.getFeatureFlagsOverride())
                    }
                })
        }
    }

    private fun bindActionOne(actionText: String?, callback: () -> Unit?) {
        if (actionText.isNullOrEmpty()) {
            binding.actionOption1.visible = false
        } else {
            binding.actionOption1.text = actionText
            binding.actionOption1.visible = true
            binding.actionOption1.click { callback() }
        }
    }

    private fun bindActionTwo(actionText: String?, callback: () -> Unit?) {
        if (actionText.isNullOrEmpty()) {
            binding.actionOption2.visible = false
        } else {
            binding.actionOption2.text = actionText
            binding.actionOption2.visible = true
            binding.actionOption2.click { callback() }
        }
    }

    private fun bindActionThree(actionText: String?, callback: () -> Unit?) {
        if (actionText.isNullOrEmpty()) {
            binding.actionOption3.visible = false
        } else {
            binding.actionOption3.text = actionText
            binding.actionOption3.visible = true
            binding.actionOption3.click { callback() }
        }
    }

    private fun bindToggle(actionText: String?, callback: (Boolean) -> Unit?) {
        if (actionText.isNullOrEmpty()) {
            binding.toggleOption1.visible = false
        } else {
            binding.toggleOption1.text = actionText
            binding.toggleOption1.visible = true
            binding.toggleOption1.isChecked = testingManager?.getToggleState() ?: false
            binding.toggleOption1.checkChange {
                testingManager?.setToggleState(it)
                callback(it)
            }
        }
    }

    private fun bindToggleTwo(actionText: String?, callback: (Boolean) -> Unit?) {
        if (actionText.isNullOrEmpty()) {
            binding.toggleOption2.visible = false
        } else {
            binding.toggleOption2.text = actionText
            binding.toggleOption2.visible = true
            binding.toggleOption2.isChecked = testingManager?.getToggleTwoState() ?: false
            binding.toggleOption2.checkChange {
                testingManager?.setToggleTwoState(it)
                callback(it)
            }
        }
    }

    private fun bindSelector() {
        with(binding) {
            if (selectorTitle.isNullOrEmpty() || selectorList.isNullOrEmpty()) {
                actionSelector.visible = false
                toggleSelector.visible = false
            } else {
                actionSelector.visible = true
                toggleSelector.visible = true
                actionSelector.text = actionSelectorText
                actionSelector.click {
                    if (!toggleSelector.isChecked) return@click
                    activity?.let { act ->
                        val dialog = DialogSelector()
                        dialog.onDismiss = { selectorCallbackState() }
                        dialog.isCancelable = false
                        dialog.arguments = bundleOf(
                            Pair(DIALOG_TITLE, selectorTitle),
                            Pair(DIALOG_LIST, selectorList)
                        )

                        dialog.show(act.supportFragmentManager, FeatureOverrideAddDialog::javaClass.name)
                    }
                }
            }
        }
    }

    private fun adjustDialog() {
        val window = dialog?.window
        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        window?.setGravity(Gravity.CENTER)
    }

    private fun hideKeyboard() {
        try {
            val imm =
                view?.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view?.windowToken, 0)
        } catch (e: Exception) {
        }
    }

    override fun onResume() {
        super.onResume()
        adjustDialog()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        hideKeyboard()
        onDismissed()
    }

    // To avoid incorrect behavior when system destroy
    override fun onPause() {
        dismiss()
        super.onPause()
    }
}
