package com.startappz.testingmanagersdk.constants

const val DIALOG_TITLE = "DIALOG_TITLE"
const val DIALOG_LIST = "DIALOG_LIST"
const val DIALOG_SELECTOR_PREF = "DIALOG_SELECTOR_PREF"
const val TOGGLE_SELECTOR_STATE = "TOGGLE_SELECTOR_STATE"
