package com.startappz.testingmanagersdk

import android.content.Context
import androidx.fragment.app.FragmentManager
import com.startappz.testingmanagersdk.constants.DIALOG_SELECTOR_PREF
import com.startappz.testingmanagersdk.constants.TOGGLE_SELECTOR_STATE
import com.startappz.testingmanagersdk.helpers.PreferenceHelper
import com.startappz.testingmanagersdk.helpers.PreferenceHelper.get
import com.startappz.testingmanagersdk.helpers.PreferenceHelper.remove
import com.startappz.testingmanagersdk.helpers.PreferenceHelper.set

class TestingManagerImp(
    private val context: Context,
    private val overrideFeaturesEnabled: Boolean,
    private val featureBaseUrl: Boolean
) : TestingManager {
    private var features: List<String> = listOf()

    // region MockApi
    private val KEY_API_MOCK_VERSION = "testing.manager.api.mock.version"
    private val KEY_API_MOCK_ENABLED = "testing.manager.api.mock.enabled"
    private val KEY_OVERRIDE_BASE_URL_ENABLED = "testing.manager.base.url.enabled"
    private val KEY_GET_OVERRIDE_BASE_URL = "testing.manager.get.base.url"
    // endregion

    // region Optional items
    private var actionText1: String? = null
    private var action1: () -> Unit = {}
    private var actionText2: String? = null
    private var action2: () -> Unit = {}
    private var actionText3: String? = null
    private var action3: () -> Unit = {}
    private var actionTextToggle: String? = null
    private var actionTextToggleTwo: String? = null
    private var actionToggle: (Boolean) -> Unit = {}
    private var actionToggleTwo: (Boolean) -> Unit = {}
    private var actionTextSelector: String? = null

    private var keyTogglePreference: String = "key.toggle.preference"
    private var keyToggleTwoPreference: String = "key.toggleTow.preference"

    // Titles
    private var selectorTitle: String? = null

    // Selector
    private var selectorList: List<String>? = null
    private var selectorCallbackState: () -> Unit = {}

    private var testingManagerDialog: TestingManagerDialog? = null

    //endregion

    // region feature override methods
    private val KEY_FEATURE_OVERRIDE_ENABLED = "testing.manager.feature.override.enabled"

    //endregion
    companion object {
        val KEY_FEATURE_OVERRIDE_PREFIX = "FO#$# "
    }

    override fun bindFeatures(features: List<String>) {
        this.features = features
    }

    override fun showTestingManager(fragmentManager: FragmentManager) {
        if (testingManagerDialog != null) {
            return
        }
        testingManagerDialog =
            TestingManagerDialog().bind(
                this,
                actionText1 = actionText1,
                action1 = action1,
                actionText2 = actionText2,
                action2 = action2,
                actionText3 = actionText3,
                action3 = action3,
                actionTextToggle = actionTextToggle,
                actionToggle = actionToggle,
                actionToggleTwo = actionToggleTwo,
                actionTextToggleTwo = actionTextToggleTwo,
                selectorTitle = selectorTitle,
                actionSelectorText = actionTextSelector,
                selectorList = selectorList,
                selectorCallbackState = selectorCallbackState
            ) { testingManagerDialog = null }
        testingManagerDialog?.show(fragmentManager, null)
    }

    override fun isFeatureOverride(featureFlag: String): Boolean {
        return PreferenceHelper.defaultPrefs(context)[KEY_FEATURE_OVERRIDE_ENABLED, false] && PreferenceHelper.defaultPrefs(
            context
        ).get<Map<String, Boolean>>(KEY_FEATURE_OVERRIDE_PREFIX)
            .containsKey(KEY_FEATURE_OVERRIDE_PREFIX + featureFlag)
    }

    override fun isFeatureOverrideAllowed(): Boolean {
        return overrideFeaturesEnabled
    }

    override fun getFeatureFlagState(featureFlag: String): Boolean {
        return PreferenceHelper.defaultPrefs(context)[KEY_FEATURE_OVERRIDE_PREFIX + featureFlag, false]
    }

    override fun isMockApiEnabled(): Boolean {
        return PreferenceHelper.defaultPrefs(context)[KEY_API_MOCK_ENABLED, false]
    }

    override fun getApiMockType(): MockType {
        return MockType.VALUE_API_MOCK
    }

    override fun getApiMockVersion(): String {
        return PreferenceHelper.defaultPrefs(context)[KEY_API_MOCK_VERSION, ""]
    }

    override fun isBaseUrlOverride(): Boolean {
        return PreferenceHelper.defaultPrefs(context)[KEY_OVERRIDE_BASE_URL_ENABLED, false]
    }

    override fun isFeatureBaseURLEnabled(): Boolean {
        return featureBaseUrl
    }

    override fun getBaseUrl(): String {
        return PreferenceHelper.defaultPrefs(context)[KEY_GET_OVERRIDE_BASE_URL, ""]
    }

    override fun bindActionOne(actionText: String, callback: () -> Unit) {
        actionText1 = actionText
        action1 = callback
    }

    override fun bindActionTwo(actionText: String, callback: () -> Unit) {
        actionText2 = actionText
        action2 = callback
    }

    override fun bindActionThree(actionText: String, callback: () -> Unit) {
        actionText3 = actionText
        action3 = callback
    }

    override fun bindToggle(actionText: String, keyTogglePreference: String, callback: (Boolean) -> Unit) {
        actionTextToggle = actionText
        actionToggle = callback
        this.keyTogglePreference = keyTogglePreference
    }

    override fun bindToggleTwo(
        actionText: String,
        keyTogglePreference: String,
        callback: (Boolean) -> Unit
    ) {
        actionTextToggleTwo = actionText
        actionToggleTwo = callback
        this.keyToggleTwoPreference = keyToggleTwoPreference
    }

    override fun bindSelector(actionText: String, title: String, list: List<String>, callbackState: () -> Unit) {
        actionTextSelector = actionText
        this.selectorTitle = title
        this.selectorList = list
        this.selectorCallbackState = callbackState
    }

    override fun isSelectorEnabled(): Boolean {
        return PreferenceHelper.defaultPrefs(context)[TOGGLE_SELECTOR_STATE, false]
    }

    override fun getSelectorItem(): String {
        return PreferenceHelper.defaultPrefs(context)[DIALOG_SELECTOR_PREF, ""]
    }

    override fun getToggleState(): Boolean {
        return PreferenceHelper.defaultPrefs(context)[keyTogglePreference, false]
    }

    override fun setToggleState(state: Boolean) {
        PreferenceHelper.defaultPrefs(context)[keyTogglePreference] = state
    }

    override fun setFeatureOverrideState(enabled: Boolean) {
        PreferenceHelper.defaultPrefs(context)[KEY_FEATURE_OVERRIDE_ENABLED] = enabled
    }

    override fun setToggleTwoState(state: Boolean) {
        PreferenceHelper.defaultPrefs(context)[keyToggleTwoPreference] = state
    }

    override fun getToggleTwoState(): Boolean {
        return PreferenceHelper.defaultPrefs(context)[keyToggleTwoPreference, false]
    }

    override fun isFeatureOverrideEnabled(): Boolean {
        return PreferenceHelper.defaultPrefs(context)[KEY_FEATURE_OVERRIDE_ENABLED, false] && overrideFeaturesEnabled
    }

    override fun setFeatureFlagState(featureFlag: String, state: Boolean) {
        PreferenceHelper.defaultPrefs(context)[KEY_FEATURE_OVERRIDE_PREFIX + featureFlag] = state
    }

    override fun getFeatureFlagsOverride(): Map<String, Boolean> {
        return PreferenceHelper.defaultPrefs(context)[KEY_FEATURE_OVERRIDE_PREFIX]
    }

    override fun removeFeatureFlagOverride(featureFlag: String) {
        PreferenceHelper.defaultPrefs(context).remove(KEY_FEATURE_OVERRIDE_PREFIX + featureFlag)
    }

    override fun setMockApiState(enabled: Boolean) {
        PreferenceHelper.defaultPrefs(context)[KEY_API_MOCK_ENABLED] = enabled
    }

    override fun setApiMockVersion(version: String?) {
        PreferenceHelper.defaultPrefs(context)[KEY_API_MOCK_VERSION] = version
    }

    override fun getFeatures(): List<String> {
        return features
    }

    override fun setBaseURLOverrideEnabled(state: Boolean) {
        PreferenceHelper.defaultPrefs(context)[KEY_OVERRIDE_BASE_URL_ENABLED] = state
    }

    override fun setBaseURL(baseURL: String) {
        PreferenceHelper.defaultPrefs(context)[KEY_GET_OVERRIDE_BASE_URL] = baseURL
    }
}

enum class MockType(val value: String) {
    VALUE_API_MOCK("Mock-Version")
}
