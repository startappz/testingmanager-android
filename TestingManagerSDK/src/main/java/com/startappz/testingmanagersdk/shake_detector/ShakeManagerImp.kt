package com.startappz.testingmanagersdk.shake_detector

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager

class ShakeManagerImp : ShakeManager {
    private var mSensorManager: SensorManager? = null
    private var mAccelerometer: Sensor? = null
    private var mShakeDetector: ShakeDetector? = null

    override fun bind(context: Context, callback: () -> Unit) {
        mSensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mAccelerometer = mSensorManager!!
            .getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mShakeDetector = ShakeDetector()
        mShakeDetector?.setOnShakeListener(object : ShakeDetector.OnShakeListener {
            override fun onShake(count: Int) {
                callback()
            }
        })
    }

    override fun register() {
        mSensorManager?.registerListener(
            mShakeDetector,
            mAccelerometer,
            SensorManager.SENSOR_DELAY_UI
        );
    }

    override fun unregister() {
        mSensorManager?.unregisterListener(mShakeDetector);
    }
}