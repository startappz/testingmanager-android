package com.startappz.testingmanagersdk.shake_detector

import android.content.Context

interface ShakeManager {
    fun bind(context: Context, callback: () -> Unit)
    fun register()
    fun unregister()
}