package com.startappz.testingmanagersdk.controller

import com.airbnb.epoxy.EpoxyController
import com.startappz.testingmanagersdk.TestingManagerImp
import com.startappz.testingmanagersdk.model.featureFlagOverride

class FeatureOverrideController(private val callBack:(String, Boolean) -> Unit) : EpoxyController() {
    internal var featureFlagsOverride: Map<String, Boolean> = hashMapOf()

    override fun buildModels() {
        if (featureFlagsOverride.isNullOrEmpty()) return

        featureFlagsOverride.forEach { (key, value) ->
            featureFlagOverride {
                id(key.hashCode())
                featureFlag(key.replace(TestingManagerImp.KEY_FEATURE_OVERRIDE_PREFIX, ""))
                state(value)
                callback(this@FeatureOverrideController.callBack)
            }
        }
    }

    fun setFeatures(input: Map<String, Boolean>) {
        featureFlagsOverride = input
        requestModelBuild()
    }
}
