package com.startappz.testingmanagersdk.controller

import com.airbnb.epoxy.EpoxyController
import com.startappz.testingmanagersdk.model.featureFlag

class AvailableFeatureFlagController(var callback :(String) -> Unit = {}) :
    EpoxyController() {
    var featureFlags: List<String>? = null

    override fun buildModels() {
        if (featureFlags.isNullOrEmpty()) return

        featureFlags?.forEachIndexed { index, value ->
            featureFlag {
                id(index)
                featureFlag(value)
                callback (this@AvailableFeatureFlagController.callback)
            }
        }
    }

    fun setFlags(input: List<String>?) {
        featureFlags = input
        requestModelBuild()
    }
}