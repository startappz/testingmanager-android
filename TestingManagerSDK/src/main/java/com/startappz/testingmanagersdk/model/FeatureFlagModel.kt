package com.startappz.testingmanagersdk.model

import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.startappz.testingmanagersdk.R
import com.startappz.testingmanagersdk.base.ViewHolderBinding
import com.startappz.testingmanagersdk.databinding.EpoxyFeatureFlagBinding
import com.startappz.testingmanagersdk.extentions.click

@EpoxyModelClass
abstract class FeatureFlagModel : ViewHolderBinding<EpoxyFeatureFlagBinding>() {
    @EpoxyAttribute
    lateinit var featureFlag: String

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var callback: (String) -> Unit

    override fun EpoxyFeatureFlagBinding.bind() {
        lblFeatureFlag.text = featureFlag
        root.click { callback(featureFlag) }
    }

    override fun getDefaultLayout(): Int {
        return R.layout.epoxy_feature_flag
    }
}
