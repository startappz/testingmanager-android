package com.startappz.testingmanagersdk.model

import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.startappz.testingmanagersdk.R
import com.startappz.testingmanagersdk.base.ViewHolderBinding
import com.startappz.testingmanagersdk.databinding.EpoxyItemFeatureOverrideFlagBinding
import com.startappz.testingmanagersdk.extentions.checkChange

@EpoxyModelClass
abstract class FeatureFlagOverrideModel : ViewHolderBinding<EpoxyItemFeatureOverrideFlagBinding>() {

    @EpoxyAttribute
    lateinit var featureFlag: String

    @EpoxyAttribute
    var state: Boolean = false

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var callback: (String, Boolean) -> Unit

    override fun EpoxyItemFeatureOverrideFlagBinding.bind() {
        if (featureFlag.isEmpty()) return

        tvFeatureFlag.text = featureFlag
        cbFlagState.isChecked = state
        cbFlagState.checkChange { state -> callback(featureFlag, state) }
    }

    override fun getDefaultLayout(): Int {
        return R.layout.epoxy_item_feature_override_flag
    }
}
